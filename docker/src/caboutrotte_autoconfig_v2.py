from os import getenv
import logging
import requests
import subprocess
from subprocess import Popen, PIPE
import re
import json


cab_url = '127.0.0.1:9013'
kub_context = 'akwok-mlf-eks-dev'
h_filename = 'log/healthchecks_list.log'
ah_filename = 'log/ah_list.log'
rh_filename = 'log/rh_list.log'

# kub_context = 'bato' to see if webservices will return an error


def webservices_list():
    bashCmdNamespace = f"kubectl --context {kub_context} get --all-namespaces ingressroutes"
    #cette partie allant jusqu'à outlist_final permet d'avoir une pair, name et namespace
    bashresults = Popen(bashCmdNamespace,shell=True,stdin=PIPE, stdout=PIPE, stderr=PIPE)
    output, stderr = bashresults.communicate()
    #vérifie s'il y'a erreur. Si oui, la fonction s'arrête et retourne l'erreur
    stderr = stderr.decode('utf-8')
    if stderr:
        return stderr
    outwithoutspace = re.sub('\s+',' ',output.decode('utf-8'))
    outlist = outwithoutspace.split(' ')
    outlist = outlist[3:]
    #etape supprimant les valeurs vides dans la liste
    outlist = [val for val in outlist if val]
    outlist_final = [vale for pos,vale in enumerate (outlist) if (pos+1)%3 !=0]
    #detail
    compos_list = [outlist_final[x:x+2] for x in range(0,len(outlist_final),2)]
    
    #le compos_list à la fin de la boucle, aura après le traitement, 3 vals : namespace, name et URL
    for value in compos_list:
        bashCmdIngress = f"kubectl --context {kub_context} -n {value[0]} get -o=custom-columns=:.spec.routes[0].match ingressroute {value[1]}"
        #bashCmdIngress = f"kubectl --context akwok-mlf-eks-dev -n webtv get -o=custom-columns=:.spec.routes[0].match ingressroute webtv-mlfwordpress"

        bashingress = Popen(bashCmdIngress,shell=True,stdin=PIPE, stdout=PIPE, stderr=PIPE)
        output_yaml, stderr = bashingress.communicate()
        stderr = stderr.decode('utf-8')
        if stderr:
            return stderr
        output_yaml = output_yaml.decode('utf-8')    
    #nettoyage  des métas caractères, \ , ou | ou \n ainsi que ` et Host (comme mot)
        if re.findall("\,|\&|\|", output_yaml):
            sep = re.findall("\,|\&|\|", output_yaml)[0]
            output_yaml = output_yaml.split(sep, 1)[0]
            output_yaml = re.sub("Host\(","",output_yaml)
            output_yaml = re.sub("`","",output_yaml)
            output_yaml = output_yaml.replace('\n',' ')
        output_yaml = re.sub("Host\(","",output_yaml)
        output_yaml = re.sub("`","",output_yaml)
        output_yaml = output_yaml.replace('\n',' ')
        if re.findall("\)",output_yaml):
            output_yaml = re.sub("\)","",output_yaml)
        output_yaml = output_yaml.strip()    
        value.append(output_yaml)    
    return compos_list


def healthchecks_list(h_filename):
    log = logging.getLogger('urllib3')
    log.setLevel(logging.DEBUG)
    fh = logging.FileHandler(h_filename)
    log.addHandler(fh)
    output = requests.get(f"http://{cab_url}/healthcheck")
    #récuperation des healthchecks de cabourotte avec le name et l'url dans 1 liste    
    output = output.text[1:-2]
    json_dict = json.loads('[' + output.replace('}{', '},{') + ']')
    name_target_lst = [[json_dict[key]['name'],json_dict[key]['target']] for key,val in enumerate(json_dict)]
    return name_target_lst


#Andy_Kw: remplace data = f'{{"name":"{subl[0]}","description… par data = {{"name": subl[0],"description… et puis dans request.post utilise le paramètre json plutôt que data 
# et vire les entêtes qu'il mettra de toute facons tout seul ?

webservices = ['boo','boo','boo.com'],['baa','baa','baa.com'],['bzz','bzz','bzz.com']  
#test case 2 - not dot on the third element in any sublist
healthchecks = ['bii','bii.com'],['brr','brr.com'],['bdd','bdd.com']

def add_healthcheck(webservices, healthchecks,ah_filename):
    headers = {'Content-Type': 'application/json',}    
    healthinars_names = {vale[0] for vale in healthchecks}
    ah_return_code = []
    for subl in webservices:
        if subl[0] not in healthinars_names:
            data = f'{{"name":"{subl[0]}","description":"{subl[0]}","target":"{subl[2]}","interval":"5s","timeout": "3s","port":443,"protocol":"https","valid-status":[200]}}'
            log = logging.getLogger('urllib3')  # works
            log.setLevel(logging.DEBUG)  # needed
            fh = logging.FileHandler(ah_filename)
            log.addHandler(fh)
            results = requests.post(f"http://{cab_url}/healthcheck/http", headers=headers, data=data)
            results.text
            ah_return_code.append(results.status_code)
    return ah_return_code        

def remove_healthcheck(webservices, healthchecks,rh_filename):
    webinars_names = {vale[0] for vale in webservices}
    rh_return_code = []
    for subl in healthchecks:
        if subl[0] not in webinars_names:
            data = subl[0]
            log = logging.getLogger('urllib3')  # works
            log.setLevel(logging.DEBUG)  # needed
            fh = logging.FileHandler(rh_filename)
            log.addHandler(fh)            
            results = requests.delete(f"http://{cab_url}/healthcheck/{data}")
            results.text
            rh_return_code.append(results.status_code)
    return rh_return_code                                            

#healthchecks = healthchecks_list(h_filename)
#webservices = [['boo','boo','boo.com'],['baa','baa','baa.com'],['bzz','bzz','bzz.com']] 


'''


stdout = subprocess.check_output(cmd, universal_newlines=True, stderr=subprocess.STDOUT) ?

import logging
import requests

class Client:
   def ___init__(self):
        self.session = requests.Session()
       
   def get(self, *args, **kwargs):
        logger.info("calling get with", args, kwargs)
        response = self.session.get(*args, **kwargs)
        response.raise_for_status()
        return response


errs = []
for f in funcs:
    try:
        f()
    except Exception as e:
        errs.append(e)
if errs:
    raise MultipleErrorsOcurredError(errs)        
'''        
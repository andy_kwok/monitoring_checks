import unittest



from cabourotte_autoconfig import ingress_cleaning
from cabourotte_autoconfig import routes_cleaning
from cabourotte_autoconfig import add_healthcheck
from cabourotte_autoconfig import remove_healthcheck


class TestCabourotte(unittest.TestCase):


    def test_ingress_cleaning(self):
        self.cleaned_ingress = ingress_cleaning('NAMESPACE                       NAME                                                           AGE\naberdeentotalgepi               aberdeentotalgepi-mlfgepi                                      167d\naeropostaleosui                 aeropostaleosui-mlfwordpress                                   167d\n')
        self.final_result_ingress = [['aberdeentotalgepi','aberdeentotalgepi-mlfgepi'],['aeropostaleosui','aeropostaleosui-mlfwordpress']]
        assert self.cleaned_ingress == self.final_result_ingress

    def test_routes_cleaning(self):
        self.cleaned_routes = routes_cleaning([['aberdeentotalgepi', 'aberdeentotalgepi-mlfgepi', '\nHost(`aberdeentotalgepi.dev.mlfmonde.org`)\n'], ['aeropostaleosui', 'aeropostaleosui-mlfwordpress', '\nHost(`aeropostaleosui.dev.mlfmonde.org`)\n']])
        self.final_result_routes = [['aberdeentotalgepi', 'aberdeentotalgepi-mlfgepi', 'aberdeentotalgepi.dev.mlfmonde.org'], ['aeropostaleosui', 'aeropostaleosui-mlfwordpress', 'aeropostaleosui.dev.mlfmonde.org']]
        assert self.cleaned_routes == self.final_result_routes


    def test_add_healthcheck(self):
        self.added_healthcheck = add_healthcheck([['so', 'so', 'stackoverflow.com'],['gg', 'gg', 'google.com']],[['badgesmlfmonde', 'badges.mlfmonde.org'], ['lfijeancharcot', 'lyceefrancaisinternationaljeancharcot.org'], ['nextcloud', 'nuage.mlfmonde.org']])
        self.final_result_added_healtcheck = '201:{"name":"so","description":"so","target":"stackoverflow.com","interval":"5s","timeout": "3s","port":443,"protocol":"https","valid-status":[200]}, 201:{"name":"gg","description":"gg","target":"google.com","interval":"5s","timeout": "3s","port":443,"protocol":"https","valid-status":[200]}'
        assert self.final_result_added_healtcheck == self.final_result_added_healtcheck


    def test_remove_healthcheck(self):
        self.removed_healtcheck = remove_healthcheck([['so', 'so', 'stackoverflow.com'],['gg', 'gg', 'google.com']],[['badgesmlfmonde', 'badges.mlfmonde.org'], ['lfijeancharcot', 'lyceefrancaisinternationaljeancharcot.org'], ['nextcloud', 'nuage.mlfmonde.org']])
        self.final_result_removed_healtcheck = '200:{"message":"Successfully deleted healthcheck badgesmlfmonde"},200:{"message":"Successfully deleted healthcheck lfijeancharcot"},200:{"message":"Successfully deleted healthcheck nextcloud"}'  
        print("removed",self.removed_healtcheck)
        print("final",self.final_result_removed_healtcheck)
        assert self.removed_healtcheck == self.final_result_removed_healtcheck

if __name__ == '__main__':
    unittest.main()    

"""
ingress_cleaning = ingress_cleaning(kubectl_ingress)
webservices = routes_cleaning(kubectl_routes)
healthchecks = healthchecks_list()
"""


"""
KUBECTL_INGRESS = 'NAMESPACE                       NAME                                                           AGE\naberdeentotalgepi               aberdeentotalgepi-mlfgepi                                      160d\naeropostaleosui                 aeropostaleosui-mlfwordpress                                   161d\n'

KUBECTL_ROUTES = '\nHost(`webtv.dev.mlfmonde.org`)\n'


HEALTHCHECKS='[['nextcloud', 'nuage.mlfmonde.org'], ['lmslfalex', 'ent.lfalex.org']]'


def mock_get_raw_kubectl_ingress():
    return KUBECTL_INGRESS

def mock_get_raw_kubectl_routes():
    return KUBECTL_ROUTES

def mock_get_raw_healthchecks():
    return HEALTHCHECKS

class TestKubes(unittest.TestCase):
        @patch(
        'cabourotte_autoconfig.get_raw_kubectl_ingress',
        return_value=KUBECTL_INGRESS
    )
    def test_ingress_cleaning(self, ingress_cleaning):
        public_ip = ingress_cleaning(ingress_cleaning) #fonction testée 
        self.assertEqual(public_ip, '[['aberdeentotalgepi', 'aberdeentotalgepi-mlfgepi', 'aberdeentotalgepi.dev.mlfmonde.org'], ['aeropostaleosui', 'aeropostaleosui-mlfwordpress', 'aeropostaleosui.dev.mlfmonde.org']]')



'''
#test case 1 - less than 3 elements per sublist
case1 = ['boo','boo','boo.com'],['baa','baa','baa.com'],['bzz','bzzcom']  
#test case 2 - not dot on the third element in any sublist
case2 = ['boo','boo','boo.com'],['baa','baa','baa.com'],['bzz','bzz','bzzcom'] 

'''

def test_check_weblist_three_elems_plus_dot(webservices):
    #only output
    assert all(isinstance(item, list) and len(item) == 3 for item in webservices)
    assert all(re.findall("[.]", item[2]) for item in webservices)


'''
#test case 1 - less than 2 elements per sublist
case1 = ['boo','boo.com'],['baa','baa.com'],['bzz']  
#test case 2 - not dot on the second element in any sublist
case2 = ['boo','boo.com'],['baa','baa.com'],['bzz','bzzcom'] 

'''

def test_check_healthchecks_two_elems_plus_dot(healthchecks):
    #only output
    assert all(isinstance(item, list) and len(item) == 2 for item in healthchecks)
    assert all(re.findall("[.]", item[1]) for item in healthchecks)

'''

in_mockweb = ['boo','boo.com'],['baa','baa.com'],['bzz']  
in_mockhealth = ['boo','boo.com'],['baa','baa.com'],['bzz','bzzcom'] 

out_mockweb = ['boo','boo','boo.com'],['baa','baa','baa.com'],['bzz','bzz','bzz.com']  
out_mockhealth = ['bii','bii.com'],['brr','brr.com'],['bdd','bdd.com']

'''    

def test_check_add_h(in_mockweb, in_mockhealth,out_mockweb,out_mockhealth):
    #input
    assert all(isinstance(item, list) and len(item) == 2 for item in in_mockhealth)
    assert all(isinstance(item, list) and len(item) == 3 for item in in_mockweb)
    assert all(re.findall("[.]", item[2]) for item in in_mockweb)
    assert all(re.findall("[.]", item[1]) for item in in_mockhealth)
    #output
    results = add_healthcheck(out_mockweb, out_mockhealth,ah_filename)
    assert '4' not in results,"pass"
    assert '5' not in results,"pass"

def test_check_rem_h(in_mockweb, in_mockhealth,out_mockweb,out_mockhealth):
    #input
    assert all(isinstance(item, list) and len(item) == 2 for item in in_mockhealth)
    assert all(isinstance(item, list) and len(item) == 3 for item in in_mockweb)    
    assert all(re.findall("[.]", item[2]) for item in in_mockweb)
    assert all(re.findall("[.]", item[1]) for item in in_mockhealth)
    #output
    results = remove_healthcheck(out_mockweb, out_mockhealth,ah_filename)
    assert '4' not in results,"pass"
    assert '5' not in results,"pass"


if __name__ == '__main__':
    unittest.main()
"""    